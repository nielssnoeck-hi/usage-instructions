import { Component } from '@angular/core';
import { FormArray, FormBuilder } from "@angular/forms";
import { Observable } from "rxjs";

import { FrequencyModel } from "./core/models/frequency.model";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  /* These have to be moved to the according components
  usageInstructionsForm = this.formBuilder.group({
    frequency: [''],
    dosages: this.formBuilder.array([])
  });

  get dosages() {
    return this.usageInstructionsForm.get('dosages') as FormArray
  }

  filteredFrequencies$: Observable<FrequencyModel[]>;
  */

  constructor(private formBuilder: FormBuilder) {
  }

}
