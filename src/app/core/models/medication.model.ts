import { DosageModel } from "./dosage.model";

export interface MedicationModel {
  frequencyId: number;
  dosages: DosageModel[]
}
