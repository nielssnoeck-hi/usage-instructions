import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";

import { DosageModel } from "../models/dosage.model";
import { FrequencyModel } from "../models/frequency.model";


@Injectable()
export class MetadataService {

  private static readonly frequencies: FrequencyModel[] = [
    { id: 1, name: 'Once a week'},
    { id: 2, name: 'Twice a week'},
    { id: 3, name: 'Three times a week'}
  ];

  private static readonly dosages: {[index: number]: DosageModel[]} = {
    1: [{quantity: 5}],
    2: [{quantity: 2}, {quantity: 3}],
    3: [{quantity: 2}, {quantity: 2}, {quantity: 1}]
  };

  /**
   * Get a list of frequencies
   *
   * @param filter
   *     A filter to apply to the frequency name.
   * @return
   *     An observable with a list of frequencies of which the
   *     name starts with the provided filter.
   */
  getFrequencies(filter = ''): Observable<FrequencyModel[]> {
    return of(
      MetadataService.frequencies.filter(frequency => frequency.name.startsWith(filter))
    );
  }

  /**
   * Get a list of dosages for a frequency
   *
   * @param frequencyId
   *    The ID of the frequency to get the dosages for.
   * @return
   *    An observable with a list of dosages for the provided frequency.
   */
  getDosages(frequencyId: number): Observable<DosageModel[]> {
    return of(
      MetadataService.dosages[frequencyId] || []
    )
  }

}
